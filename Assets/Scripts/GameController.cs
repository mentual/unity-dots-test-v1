using DI;
using Enemy;
using Nexus;
using Player;
using UI;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    public const string GAME_RECORD_KEY = "GAME_RECORD";

    [DI_Inject] private NexusGlobalController nexusGlobalController;
    [DI_Inject] private EnemyController enemyController;
    [DI_Inject] private UIController uiController;
    [DI_Inject] private PlayerController playerController;

    [SerializeField] private int waveTime;

    private float gameTimer;
    private float waveTimer;
    private bool canSpawn;

    // for debug view
    [SerializeField] private bool isGameOver;
    [SerializeField] private int wave;

    /// <summary>
    /// int = wave No
    /// string = wave enemies name
    /// </summary>
    public UnityAction<int, string> OnWaveStarted;


    public bool IsGameOver
    {
        get => isGameOver;
        private set => isGameOver = value;
    }


    #region Mono

    private void Awake()
    {
        DIContainer.Instance.Register(this);
    }

    private void Start()
    {
        DIContainer.Instance.Fetch(this);
        
        Physics.autoSimulation = false;
        Physics2D.simulationMode = SimulationMode2D.Script;

        nexusGlobalController.OnAllNexusesDestroyed += GameOver;

        ResetGame();
    }

    private void Update()
    {
        if (isGameOver && Input.GetKeyUp(KeyCode.R))
            ResetGame();

        if (isGameOver)
            return;

        if (Input.GetKeyDown(KeyCode.Return))
            StartWave();

        // game not started yet
        if (wave < 1)
            return;

        gameTimer += Time.deltaTime;
        uiController.UpdateTimer(gameTimer);

        waveTimer -= Time.deltaTime;
        uiController.UpdateNextWave(waveTimer, wave);

        if (waveTimer < 0)
            StartWave();
    }

    private void OnDestroy()
    {
        DIContainer.Instance.CleanContainer();
    }

    #endregion


    public void StartWave()
    {
        //reset Wave Timer
        waveTimer = waveTime;

        wave++;

        if (wave == 1)
            uiController.OnGameStart();

        OnWaveStarted?.Invoke(wave, enemyController.enemyPrefabName);
    }

    private void ResetGame()
    {
        wave = 0;
        isGameOver = false;
        gameTimer = 0f;
        waveTimer = 0f;

        playerController.Reset();
        uiController.Reset();
        enemyController.Reset();
        nexusGlobalController.Reset();
    }

    private void GameOver()
    {
        isGameOver = true;

        TrySaveHighscore();

        var currentRecord = PlayerPrefs.GetFloat(GAME_RECORD_KEY, 0f);

        uiController.SetGameOverUI(currentRecord);
    }

    private void TrySaveHighscore()
    {
        var currentRecord = PlayerPrefs.GetFloat(GAME_RECORD_KEY, 0f);

        if (gameTimer > currentRecord)
        {
            PlayerPrefs.SetFloat(GAME_RECORD_KEY, gameTimer);
        }
    }


    public int GetWaveNo()
    {
        return wave;
    }
}