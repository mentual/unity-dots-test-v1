using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Utils
{
    public static class AddressableHelper
    {
        public static async UniTask<T> LoadAddressableTask<T>(string path, CancellationTokenSource token)
        {
            try
            {
                T item = await Addressables.LoadAssetAsync<T>(path);

                Debug.Log("Successfully Loaded Object from path: " + path);

                return item;
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("Object for path {0} couldn't be loaded! with exception: {1}", path, ex);
                token?.Cancel();
                return default;
            }
        }
    }
}