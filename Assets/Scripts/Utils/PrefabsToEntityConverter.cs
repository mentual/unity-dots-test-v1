using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Events;

namespace Utils
{
    public class PrefabsToEntityConverter : MonoBehaviour, IDeclareReferencedPrefabs, IConvertGameObjectToEntity
    {
        private List<GameObject> prefabs;
        private Dictionary<string, Entity> entitiesMap;

        public UnityAction added;

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            for (int i = 0; i < prefabs.Count; i++)
            {
                var prefabEntity = conversionSystem.GetPrimaryEntity(prefabs[i]);
                entitiesMap.Add(prefabs[i].name, prefabEntity);
            }

            dstManager.DestroyEntity(entity);
            
            added?.Invoke();
        }

        public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
        {
            referencedPrefabs.AddRange(prefabs);
        }


        public void ProcessPrefabs(List<GameObject> prefabsToProcess, ref Dictionary<string, Entity> entities)
        {
            prefabs = prefabsToProcess;
            entitiesMap = entities;
        }
        
    }
}