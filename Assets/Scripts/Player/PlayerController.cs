using System.Collections;
using DI;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        [DI_Inject] private GameController gameController;

        [SerializeField] private Camera mainCam;

        [SerializeField] private Transform swordPivot;
        [SerializeField] private Transform swordStart;
        [SerializeField] private Transform swordEnd;
        [SerializeField] private float swordThickness;
        [SerializeField] private float swordLengthMult;
        [SerializeField] private TrailRenderer swordTrail;
        [SerializeField] private float swingTime;
        [SerializeField] private float swingAngle;
        [SerializeField] private float sashAngle;

        [SerializeField] private float playerSpeed;
        [SerializeField] private Vector2 moveBounds;

        private Coroutine swingCoroutine;
        private bool isSwordSwinging;
        public bool IsSwordSwinging => isSwordSwinging;

        private float swordLength;


        #region MONO

        private void Awake()
        {
            DIContainer.Instance.Register(this);
        }

        private void Start()
        {
            DIContainer.Instance.Fetch(this);

            swordLength = Vector3.Distance(swordEnd.position, swordStart.position) * swordLengthMult;
            swordTrail.widthMultiplier = swordLength;

            BackToSash();
        }

        private void Update()
        {
#if UNITY_EDITOR
            DrawSwordBoundsDebug();
#endif

            if (gameController.IsGameOver)
                return;

            SwordUpdate();
        }

        #endregion


        public void Reset()
        {
            transform.position = Vector3.zero;
        }

        private void SwordUpdate()
        {
            if (!isSwordSwinging && Input.GetMouseButtonDown(0))
            {
                if (!ReferenceEquals(swingCoroutine, null))
                    StopCoroutine(swingCoroutine);

                swingCoroutine = StartCoroutine(SwingSword());
            }

            var x = Input.GetAxisRaw("Horizontal");
            var y = Input.GetAxisRaw("Vertical");

            var normalizedDir = new Vector3(x, y, 0f).normalized;
            var step = normalizedDir * (playerSpeed * Time.deltaTime);

            var newPos = transform.position + step;
            if (IsInBounds(newPos))
                transform.position = newPos;

            if (!isSwordSwinging)
            {
                Vector2 mousePosInWorld = mainCam.ScreenToWorldPoint(Input.mousePosition);
                Vector2 lookDir = (mousePosInWorld - (Vector2) transform.position).normalized;

                transform.up = lookDir;
            }
        }

        private bool IsInBounds(Vector2 newPos)
        {
            return newPos.x > -moveBounds.x && newPos.x < moveBounds.x && newPos.y > -moveBounds.y &&
                   newPos.y < moveBounds.y;
        }

        private IEnumerator SwingSword()
        {
            isSwordSwinging = true;
            swordTrail.emitting = true;
            swordTrail.time = swingTime / 3f;

            float timer = 0f;
            float anglePerTick = swingAngle / swingTime;
            while (timer < swingTime)
            {
                timer += Time.deltaTime;
                float angleToMove = anglePerTick * Time.deltaTime;
                swordPivot.Rotate(Vector3.forward, angleToMove);

                yield return null;
            }

            swordTrail.emitting = false;
            swordTrail.time = 0f;

            BackToSash();
        }

        private void BackToSash()
        {
            swordPivot.localEulerAngles = new Vector3(0, 0, sashAngle);

            isSwordSwinging = false;
        }

        public Vector2 GetSwordAPoint()
        {
            var forward = swordStart.up;
            var right = swordStart.right;
            return -forward * swordLength + right * swordThickness + swordEnd.position;
        }

        public Vector2 GetSwordBPoint()
        {
            var forward = swordStart.up;
            var right = swordStart.right;
            return forward * swordLength + right * swordThickness + swordStart.position;
        }

        public Vector2 GetSwordCPoint()
        {
            var forward = swordStart.up;
            var right = swordStart.right;
            return forward * swordLength - right * swordThickness + swordStart.position;
        }

        public Vector2 GetSwordDPoint()
        {
            var forward = swordStart.up;
            var right = swordStart.right;
            return -forward * swordLength - right * swordThickness + swordEnd.position;
        }


        private void DrawSwordBoundsDebug()
        {
            Vector2 swordA = GetSwordAPoint();
            Vector2 swordB = GetSwordBPoint();
            Vector2 swordC = GetSwordCPoint();
            Vector2 swordD = GetSwordDPoint();

            Debug.DrawLine(swordA, swordB, Color.red);
            Debug.DrawLine(swordB, swordC, Color.green);
            Debug.DrawLine(swordC, swordD, Color.blue);
            Debug.DrawLine(swordD, swordA, Color.cyan);
        }
    }
}