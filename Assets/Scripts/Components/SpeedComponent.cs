using Unity.Entities;

namespace Components
{
    [GenerateAuthoringComponent]
    public struct SpeedComponent : IComponentData
    {
        public float speed;
    }
}