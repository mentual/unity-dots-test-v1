using Unity.Entities;
using Unity.Mathematics;

namespace Components
{
    [GenerateAuthoringComponent]
    public struct TargetComponent : IComponentData
    {
        public int targetIndex;
        public float3 target;
        public bool killedTarget;
    }
}
