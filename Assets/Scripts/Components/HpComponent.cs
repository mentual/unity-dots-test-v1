using Unity.Entities;

namespace Components
{
    [GenerateAuthoringComponent]
    public struct HpComponent : IComponentData
    {
        public int hp;
    }
}