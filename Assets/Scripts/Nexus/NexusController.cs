using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Nexus
{
    public class NexusController : MonoBehaviour
    {
        [SerializeField] private TextMeshPro hpText;
        [SerializeField] private int startingHp;

        [SerializeField] bool isDestroyed;
        private int currentHp;

        public UnityAction OnNexusDestroyed;
        public int index;


        #region MONO

        private void Start()
        {
            currentHp = startingHp;
            UpdateHpText();
        }

        #endregion

        public bool IsActive()
        {
            return !isDestroyed;
        }

        public void DamageNexus(int damage)
        {
            if (isDestroyed)
                return;

            currentHp -= damage;

            if (currentHp <= 0)
            {
                currentHp = 0;
                DestroyNexus();
            }

            UpdateHpText();
        }

        void DestroyNexus()
        {
            isDestroyed = true;
            OnNexusDestroyed?.Invoke();
        }


        void UpdateHpText()
        {
            hpText.text = currentHp.ToString();
        }
    }
}