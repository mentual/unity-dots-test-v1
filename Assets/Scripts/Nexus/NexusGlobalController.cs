using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DI;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using Random = UnityEngine.Random;

namespace Nexus
{
    public class NexusGlobalController : MonoBehaviour
    {
        [SerializeField] private string nexusPrefabName;

        [Range(1, 6)] [SerializeField] private int nexusNo;
        [SerializeField] private Transform[] nexusSpawnPoints;

        private NexusController[] nexuses = new NexusController[0];
        private List<NexusController> activeNexusesList = new List<NexusController>();

        public UnityAction OnAllNexusesDestroyed;

        private NexusController nexusPrefab;


        #region Mono

        private void Awake()
        {
            DIContainer.Instance.Register(this, typeof(NexusGlobalController));
        }

        #endregion


        void CheckNexuses()
        {
            activeNexusesList.RemoveAll(x => !x.IsActive());
            if (GetActiveNexusesCount() <= 0)
            {
                OnAllNexusesDestroyed?.Invoke();
            }
        }

        public int GetActiveNexusesCount()
        {
            int activeNo = 0;

            foreach (var nexus in nexuses)
            {
                if (nexus.IsActive())
                {
                    activeNo++;
                }
            }

            return activeNo;
        }

        public NexusController GetNexus(int index)
        {
            return nexuses[index];
        }

        public Vector3 GetActiveNexusPosition(int i)
        {
            return activeNexusesList[i].transform.position;
        }

        public int GetActiveNexusIndex(int i)
        {
            return activeNexusesList[i].index;
        }

        public void Reset()
        {
            ClearNexuses();

            CreateNexuses();
        }

        private async void CreateNexuses()
        {
            if (ReferenceEquals(nexusPrefab, null))
            {
                var cToken = new CancellationTokenSource();
                var nexusPrefabObject =
                    await AddressableHelper.LoadAddressableTask<GameObject>(nexusPrefabName, cToken);
                nexusPrefab = nexusPrefabObject.GetComponent<NexusController>();
                // if wrong name/not downloaded prefab -> it can be null...
                if (cToken.IsCancellationRequested)
                    return;
            }

            List<Transform> usedNexusSpawns = nexusSpawnPoints.ToList();
            nexuses = new NexusController[nexusNo];

            for (int i = 0; i < nexusNo; i++)
            {
                nexuses[i] = Instantiate(nexusPrefab);
                nexuses[i].transform.position = RandomizeNexusPosition(usedNexusSpawns);
                nexuses[i].transform.parent = transform;
                nexuses[i].OnNexusDestroyed += CheckNexuses;
                activeNexusesList.Add(nexuses[i]);
                nexuses[i].index = i;
            }
        }

        private void ClearNexuses()
        {
            foreach (var nexusController in nexuses)
            {
                Destroy(nexusController.gameObject);
            }

            activeNexusesList.Clear();
        }

        private Vector2 RandomizeNexusPosition(List<Transform> possibleSpawnPoints)
        {
            int spawnPointIndex = Random.Range(0, possibleSpawnPoints.Count);

            var spawnPoint = possibleSpawnPoints[spawnPointIndex].position;

            possibleSpawnPoints.RemoveAt(spawnPointIndex);

            return spawnPoint;
        }
    }
}