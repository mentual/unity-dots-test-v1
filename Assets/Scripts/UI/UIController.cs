using System.Globalization;
using DI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class UIController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI entitiesCountText;
        [SerializeField] private TextMeshProUGUI gameOverText;
        [SerializeField] private TextMeshProUGUI timeText;
        [SerializeField] private TextMeshProUGUI instructionsText;
        [SerializeField] private TextMeshProUGUI timeToNextWaveText;
        [SerializeField] private TextMeshProUGUI waveNoText;
        [SerializeField] private Image infoBg;


        #region Mono

        private void Awake()
        {
            DIContainer.Instance.Register(this);
        }

        #endregion


        public void UpdateEntitiesCountText(int enemiesCount)
        {
            entitiesCountText.text = "enemies: " + enemiesCount;
        }

        public void SetGameOverUI(float gameRecord)
        {
            gameOverText.enabled = true;
            gameOverText.text =
                $"Game Over \n RECORD: {gameRecord.ToString("N2", CultureInfo.InvariantCulture)} \n \n Press 'R' to restart!";
        }

        public void UpdateTimer(float gameTimer)
        {
            timeText.text = gameTimer.ToString("N2", CultureInfo.InvariantCulture);
        }

        public void UpdateNextWave(float timer, int wave)
        {
            timeToNextWaveText.text = timer.ToString("N2", CultureInfo.InvariantCulture);
            waveNoText.text = wave.ToString($"Wave: {wave}");
        }


        public void OnGameStart()
        {
            infoBg.enabled = false;
            instructionsText.enabled = false;
        }

        public void Reset()
        {
            gameOverText.enabled = false;
            infoBg.enabled = true;
            instructionsText.enabled = true;
            UpdateEntitiesCountText(0);
            UpdateTimer(0f);
            UpdateNextWave(0f, 0);
        }
    }
}