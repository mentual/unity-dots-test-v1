using System;

namespace DI
{
   public interface IDIContainer
   {
      void Register(object obj, Type objType = null, bool forceRegister = false);
      void Fetch(object objToFill, bool forceFetch = false);
      void CleanContainer();
   }
}
