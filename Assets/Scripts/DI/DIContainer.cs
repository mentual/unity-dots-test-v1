using System;
using System.Collections.Generic;
using System.Reflection;

namespace DI
{
    public class DIContainer : IDIContainer
    {
        private static IDIContainer instance;

        public static IDIContainer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DIContainer();
                }

                return instance;
            }

            private set { instance = value; }
        }


        private readonly Dictionary<Type, object> containerDictionary = new Dictionary<Type, object>();


        public void Register(object obj, Type objType = null, bool forceRegister = false)
        {
            objType ??= obj.GetType();

            if (!containerDictionary.ContainsKey(objType) || forceRegister)
            {
                containerDictionary.Add(objType, obj);
            }
        }

        public void Fetch(object objToFill, bool forceFetch = false)
        {
            FieldInfo[] fields =
                objToFill.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var field in fields)
            {
                if (!ReferenceEquals(field.GetCustomAttributes<DI_Inject>(true), null))
                {
                    var currentVal = field.GetValue(objToFill);
                    if (ReferenceEquals(currentVal, null) || forceFetch)
                    {
                        var fieldValue = FindDependency(field.FieldType);
                        field.SetValue(objToFill, fieldValue);
                    }
                }
            }
        }

        private object FindDependency(Type type)
        {
            if (containerDictionary.ContainsKey(type))
                return containerDictionary[type];

            return null;
        }

        public void CleanContainer()
        {
            containerDictionary.Clear();
        }
    }
}