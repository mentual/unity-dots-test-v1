using Components;
using DI;
using Jobs;
using Unity.Entities;
using Unity.Transforms;

namespace Systems
{
    [UpdateBefore(typeof(HpDestroySystem))]
    public class TargetMoveJobSystem : SystemBase
    {
        private EntityQuery query;
        [DI_Inject] private GameController gameController;
        
        
        protected override void OnCreate()
        {
            query = GetEntityQuery(ComponentType.ReadOnly<TargetComponent>());
            base.OnCreate();
        }
        
        protected override void OnStartRunning()
        {
            DIContainer.Instance.Fetch(this);
        }

        protected override void OnUpdate()
        { 
            if (gameController.IsGameOver)
                return;
            
            var job = new MoveJob
            {
                deltaTime = Time.DeltaTime,
                translationComponent = GetComponentTypeHandle<Translation>(),
                rotationComponent = GetComponentTypeHandle<Rotation>(),
                speedComponent = GetComponentTypeHandle<SpeedComponent>(),
                targetComponent = GetComponentTypeHandle<TargetComponent>()
            };

            var jobHandle = job.ScheduleParallel(query, Dependency);
            jobHandle.Complete();
        }
    }
}