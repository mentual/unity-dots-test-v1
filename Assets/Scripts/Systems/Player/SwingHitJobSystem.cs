using Components;
using DI;
using Jobs;
using Player;
using Unity.Entities;
using Unity.Transforms;

namespace Systems
{
    [UpdateBefore(typeof(HpDestroySystem))]
    [UpdateAfter(typeof(EnemyKamikazeSystem))]
    [UpdateAfter(typeof(EnemyFindTargetSystem))]
    public class SwingHitJobSystem : SystemBase
    {
        [DI_Inject] private PlayerController playerController;

        private EntityQuery query;


        protected override void OnCreate()
        {
            base.OnCreate();
            query = GetEntityQuery(ComponentType.ReadOnly<EnemyComponentTag>());
        }

        protected override void OnStartRunning()
        {
            DIContainer.Instance.Fetch(this);
        }

        protected override void OnUpdate()
        {
            if (!playerController.IsSwordSwinging)
                return;

            var swordAPoint = playerController.GetSwordAPoint();
            var swordBPoint = playerController.GetSwordBPoint();
            var swordCPoint = playerController.GetSwordCPoint();

            var job = new SwingHitJob()
            {
                swordPointA = swordAPoint,
                swordPointB = swordBPoint,
                swordPointC = swordCPoint,
                targetTranslationComponent = GetComponentTypeHandle<Translation>(),
                targetHpComponent = GetComponentTypeHandle<HpComponent>()
            };

            var jobHandle = job.ScheduleParallel(query, Dependency);
            jobHandle.Complete();
        }
    }
}