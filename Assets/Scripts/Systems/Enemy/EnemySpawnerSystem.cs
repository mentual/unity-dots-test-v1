using System.Threading;
using Components;
using DI;
using Enemy;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems
{
    public class EnemySpawnerSystem : ComponentSystem
    {
        [DI_Inject] private EnemyController enemyController;
        [DI_Inject] private GameController gameController;

        protected override void OnStartRunning()
        {
            base.OnStartRunning();
            DIContainer.Instance.Fetch(this);
            gameController.OnWaveStarted += SpawnEntity;
        }

        protected override void OnUpdate()
        {
        }

        async void SpawnEntity(int waveNo, string entityName)
        {
            var token = new CancellationTokenSource();
            var entityToInstantiate = await enemyController.GetOrLoadEntity(entityName, token);

            if (token.IsCancellationRequested)
            {
                return;
            }

            int waveCount = enemyController.GetEnemiesCount(waveNo);
            for (int i = 0; i < waveCount; i++)
            {
                var ent = EntityManager.Instantiate(entityToInstantiate);

                EntityManager.SetComponentData(ent, new Translation
                {
                    Value = new float3(enemyController.GetRandomEnemySpawnPosition(), 0)
                });

                EntityManager.SetComponentData(ent, new SpeedComponent()
                {
                    speed = enemyController.GetRandomEnemySpeed(waveNo)
                });

                EntityManager.SetComponentData(ent, new HpComponent()
                {
                    hp = enemyController.GetRandomEnemyHp()
                });
            }
            
            enemyController.UpdateEntitiesCountText();
        }
    }
}