using Components;
using DI;
using Jobs;
using Nexus;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Systems
{
    [UpdateBefore(typeof(TargetMoveJobSystem))]
    public class EnemyFindTargetSystem : SystemBase
    {
        private EntityQuery query;

        [DI_Inject] private NexusGlobalController nexusGlobalController;
        [DI_Inject] private GameController gameController;


        protected override void OnCreate()
        {
            query = GetEntityQuery(ComponentType.ReadOnly<EnemyComponentTag>());
            base.OnCreate();
        }
        
        protected override void OnStartRunning()
        {
            DIContainer.Instance.Fetch(this);
        }

        protected override void OnUpdate()
        {
            if (gameController.IsGameOver)
                return;

            NativeArray<float3> potentialPositions = new NativeArray<float3>(nexusGlobalController.GetActiveNexusesCount(), Allocator.TempJob);
            NativeArray<int> potentialTargetsIndex = new NativeArray<int>(nexusGlobalController.GetActiveNexusesCount(), Allocator.TempJob);

            for (int i = 0; i < potentialPositions.Length; i++)
            {
                potentialPositions[i] = (float3) nexusGlobalController.GetActiveNexusPosition(i);
            }
            
            for (int i = 0; i < potentialPositions.Length; i++)
            {
                potentialTargetsIndex[i] = nexusGlobalController.GetActiveNexusIndex(i);
            }

            var job = new FindClosestTargetJob
            {
                translationComponent = GetComponentTypeHandle<Translation>(),
                target = GetComponentTypeHandle<TargetComponent>(),
                potentialTargetsPos = potentialPositions,
                potentialTargetsIndex = potentialTargetsIndex,
            };

            var jobHandle = job.ScheduleParallel(query, Dependency);
            jobHandle.Complete();

            potentialPositions.Dispose();
            potentialTargetsIndex.Dispose();
        }
    }
}