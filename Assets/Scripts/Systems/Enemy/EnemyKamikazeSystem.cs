using Components;
using DI;
using Jobs;
using Nexus;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Systems
{
    /// <summary>
    /// if it reaches targets nearby -> it explodes, hurting target & killing itself
    /// </summary>
    [UpdateAfter(typeof(TargetMoveJobSystem))]
    public class EnemyKamikazeSystem : SystemBase
    {
        private EntityQuery query;

        [DI_Inject] private GameController gameController;
        [DI_Inject] private NexusGlobalController nexusGlobalController;

        protected override void OnCreate()
        {
            base.OnCreate();

            query = GetEntityQuery(ComponentType.ReadOnly<EnemyComponentTag>());
        }

        protected override void OnStartRunning()
        {
            DIContainer.Instance.Fetch(this);
        }

        protected override void OnUpdate()
        {
            if (gameController.IsGameOver)
                return;

            // KAMIKAZE JOB
            var job = new EnemyKamikazeJob
            {
                hpComponent = GetComponentTypeHandle<HpComponent>(),
                targetComponent = GetComponentTypeHandle<TargetComponent>(),
                translationComponent = GetComponentTypeHandle<Translation>(),
            };

            var jobHandle = job.ScheduleParallel(query, Dependency);
            jobHandle.Complete();


            // HURT JOB
            NativeHashMap<int, int> nexusesToHurt =
                new NativeHashMap<int, int>(nexusGlobalController.GetActiveNexusesCount(), Allocator.TempJob);

            Entities.WithAll<EnemyComponentTag>().ForEach((in TargetComponent targetComponent) =>
            {
                if (targetComponent.killedTarget)
                {
                    if (nexusesToHurt.ContainsKey(targetComponent.targetIndex))
                    {
                        nexusesToHurt[targetComponent.targetIndex]++;
                    }
                    else
                    {
                        nexusesToHurt.Add(targetComponent.targetIndex, 1);
                    }
                }
            }).WithBurst().Run();

            foreach (var nexus in nexusesToHurt)
                nexusGlobalController.GetNexus(nexus.Key).DamageNexus(nexus.Value);

            nexusesToHurt.Dispose();
        }
    }
}