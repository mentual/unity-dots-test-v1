using Components;
using Jobs;
using Unity.Entities;

namespace Systems
{
    [UpdateAfter(typeof(EnemyKamikazeSystem))]
    public class HpDestroySystem : SystemBase
    {
        private EndSimulationEntityCommandBufferSystem endSimCommandBufferSystem;

        private EntityQuery query;


        protected override void OnCreate()
        {
            endSimCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            query = GetEntityQuery(ComponentType.ReadOnly<HpComponent>());

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var job = new EnemyDestroyJob
            {
                CommandBuffer = endSimCommandBufferSystem.CreateCommandBuffer().AsParallelWriter(),
                entity = GetEntityTypeHandle(),
                hpComponent = GetComponentTypeHandle<HpComponent>()
            };

            var jobHandle = job.ScheduleParallel(query, Dependency);
            jobHandle.Complete();

            endSimCommandBufferSystem.AddJobHandleForProducer(jobHandle);
        }
    }
}