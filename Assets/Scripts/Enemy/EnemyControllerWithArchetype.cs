using Components;
using Enemy;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class EnemyControllerWithArchetype : MonoBehaviour
{
    [SerializeField] int enemiesToSpawn;
    [SerializeField] Vector2 speedMinMax;
    [SerializeField] private Mesh enemyMesh;
    [SerializeField] private Material[] enemyMats;

    private EntityManager entityManager;
    private EntityArchetype enemyArchetype;

    private void Awake()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    private void Update()
    {
        // 2
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!enemyArchetype.Valid)
                SetEnemyArchetype();

            Spawn(enemiesToSpawn);
        }
    }

    void SetEnemyArchetype()
    {
        enemyArchetype = entityManager.CreateArchetype(
            typeof(HpComponent),
            typeof(SpeedComponent),
            typeof(Translation),
            typeof(Rotation),
            typeof(NonUniformScale),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(RenderBounds)
        );
    }

    void Spawn(int enemiesToSpawn)
    {
        NativeArray<Entity> enemiesArray = new NativeArray<Entity>(enemiesToSpawn, Allocator.Temp);

        entityManager.CreateEntity(enemyArchetype, enemiesArray);

        for (int i = 0; i < enemiesToSpawn; i++)
        {
            entityManager.SetComponentData(enemiesArray[i], new HpComponent {hp = 100});

            entityManager.SetComponentData(enemiesArray[i],
                new SpeedComponent() {speed = UnityEngine.Random.Range(speedMinMax.x, speedMinMax.y)});

            entityManager.SetComponentData(enemiesArray[i],
                new Translation {Value = new float3(UnityEngine.Random.Range(-8f, 8f), -5f, 0f)});

            entityManager.SetComponentData(enemiesArray[i], new RenderBounds
            {
                Value = new Bounds(Vector3.zero, Vector3.one * 1).ToAABB()
            });

            entityManager.SetComponentData(enemiesArray[i], new NonUniformScale()
            {
                Value = new float3(1f, 2f, 1f)
            });

            entityManager.SetSharedComponentData(enemiesArray[i], new RenderMesh
            {
                mesh = enemyMesh,
                material = enemyMats[UnityEngine.Random.Range(0, enemyMats.Length)]
            });
        }

        enemiesArray.Dispose();
    }
}