using System;
using System.Collections.Generic;
using System.Threading;
using Components;
using Cysharp.Threading.Tasks;
using DI;
using UI;
using Unity.Entities;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [DI_Inject] private UIController uiController;

        public string enemyPrefabName;
        [SerializeField] private int enemiesWaveZero;
        [SerializeField] private int enemiesPerWave;
        [SerializeField] private float enemySpawnRadius;
        [SerializeField] private Vector2 enemiesHpMinMax;
        [SerializeField] private Vector2 enemiesSpeedMinMax;
        [SerializeField] private float enemySpeedPerWaveMult = 1.5f;

        private Dictionary<string, Entity> entitiesMap;
        private EntityManager entityManager;
        private BlobAssetStore blobAssetStore;
        private GameObjectConversionSettings gameObjectConversionSettings;

        private int entitiesCount;


        #region Mono

        private void Awake()
        {
            entitiesMap = new Dictionary<string, Entity>();

            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

            DIContainer.Instance.Register(this);
        }

        private void Start()
        {
            DIContainer.Instance.Fetch(this);
        }

        private void Update()
        {
            UpdateEntitiesCountText();
        }

        #endregion


        async UniTask LoadMany(List<string> entities)
        {
            var tasks = new List<UniTask>();

            foreach (var entityName in entities)
            {
                tasks.Add(LoadAndConvertToEntityAsync(entityName, null));
            }

            await UniTask.WhenAll(tasks);

            foreach (var entity in entitiesMap)
            {
                Debug.Log("entityMap keys: " + entity.Key);
            }
        }

        async UniTask LoadAndConvertToEntityAsync(string prefabPath, CancellationTokenSource token)
        {
            var objectToLoad = await AddressableHelper.LoadAddressableTask<GameObject>(prefabPath, token);

            // if wrong name/not downloaded prefab -> it can be null...
            if (token.IsCancellationRequested)
                return;

            blobAssetStore = new BlobAssetStore();
            gameObjectConversionSettings =
                GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore);

            var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(objectToLoad,
                gameObjectConversionSettings);

            entitiesMap.Add(prefabPath, entity);

            blobAssetStore.Dispose();

            Debug.LogFormat("Conversion of {0} successfully completed!", prefabPath);
        }

        async UniTask LoadAndConvertToEntityAsyncMediatorAsync(string prefabPath, CancellationTokenSource token)
        {
            var objectToLoad = await AddressableHelper.LoadAddressableTask<GameObject>(prefabPath, token);

            // if wrong name/not downloaded prefab -> it can be null...
            if (token.IsCancellationRequested)
                return;

            List<GameObject> gameObjects = new List<GameObject>();
            gameObjects.Add(objectToLoad);

            bool canGo = false;

            var converterObj = new GameObject("PrefabsToEntityConverter");
            var converterComponent = converterObj.AddComponent<PrefabsToEntityConverter>();
            converterComponent.added += () => { canGo = true; };
            converterComponent.ProcessPrefabs(gameObjects, ref entitiesMap);
            converterObj.AddComponent<ConvertToEntity>().ConversionMode = ConvertToEntity.Mode.ConvertAndDestroy;

            // Conversion does take 1fps! wait for it to complete
            await UniTask.WaitUntil(() => canGo);

            Debug.LogFormat("Conversion of {0} successfully completed!", prefabPath);
        }

        public async UniTask<Entity> GetOrLoadEntity(string entityName, CancellationTokenSource token)
        {
            if (!entitiesMap.ContainsKey(entityName))
            {
                await LoadAndConvertToEntityAsync(entityName, token);
                // await LoadAndConvertToEntityAsyncMediatorAsync(entityName, token);
            }

            if (token.IsCancellationRequested)
                return default;

            return entitiesMap[entityName];
        }


        public void UpdateEntitiesCountText()
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<EnemyComponentTag>());

            entitiesCount = query.CalculateEntityCount();

            uiController.UpdateEntitiesCountText(entitiesCount);
        }

        public void Reset()
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<EnemyComponentTag>());

            entityManager.DestroyEntity(query);
        }


        #region Getters

        public int GetEnemiesCount(int waveNo)
        {
            return enemiesWaveZero + (waveNo - 1) * enemiesPerWave;
        }

        public Vector2 GetRandomEnemySpawnPosition()
        {
            var angle = Random.Range(0f, 1f);
            var x = (float) (enemySpawnRadius * Math.Sin(Math.PI * 2 * angle));
            var y = (float) (enemySpawnRadius * Math.Cos(Math.PI * 2 * angle));

            return new Vector2(x, y);
        }

        public float GetRandomEnemySpeed(int waveNo)
        {
            return Random.Range(enemiesSpeedMinMax.x, enemiesSpeedMinMax.y) *
                   (Mathf.Pow(enemySpeedPerWaveMult, waveNo));
        }

        public int GetRandomEnemyHp()
        {
            return Random.Range((int) enemiesHpMinMax.x, (int) enemiesHpMinMax.y);
        }

        #endregion
    }
}