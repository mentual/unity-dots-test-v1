using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Jobs
{
    [BurstCompile]
    public struct FindClosestTargetJob : IJobChunk
    {
        public ComponentTypeHandle<Translation> translationComponent;
        public ComponentTypeHandle<TargetComponent> target;

        [ReadOnly] public NativeArray<float3> potentialTargetsPos;
        [ReadOnly] public NativeArray<int> potentialTargetsIndex;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var chunkTranslations = chunk.GetNativeArray(translationComponent);
            var chunkTargets = chunk.GetNativeArray(target);

            for (int i = 0; i < chunk.Count; i++)
            {
                float3 closestTarget = float3.zero;
                float currentDist = math.INFINITY;

                int targetIndex = 0;
                for (int j = 0; j < potentialTargetsPos.Length; j++)
                {
                    float3 targetPos = potentialTargetsPos[j];

                    var tempDist = math.distancesq(chunkTranslations[i].Value, targetPos);
                    if (tempDist < currentDist)
                    {
                        currentDist = tempDist;

                        targetIndex = potentialTargetsIndex[j];
                        closestTarget = targetPos;
                    }
                }

                chunkTargets[i] = new TargetComponent()
                {
                    target = closestTarget,
                    targetIndex = targetIndex
                };
            }
        }
    }
}