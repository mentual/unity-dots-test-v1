using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Jobs
{
    [BurstCompile]
    public struct EnemyKamikazeJob : IJobChunk
    {
        [ReadOnly] public ComponentTypeHandle<Translation> translationComponent;
        public ComponentTypeHandle<TargetComponent> targetComponent;
        public ComponentTypeHandle<HpComponent> hpComponent;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var translations = chunk.GetNativeArray(translationComponent);
            var targets = chunk.GetNativeArray(targetComponent);
            var hps = chunk.GetNativeArray(hpComponent);

            var count = chunk.Count;
            const float distanceSqrtThreshold = 0.12f;

            for (int i = 0; i < count; i++)
            {
                if (math.distancesq(translations[i].Value, targets[i].target) < distanceSqrtThreshold)
                {
                    hps[i] = new HpComponent()
                    {
                        hp = 0
                    };

                    targets[i] = new TargetComponent()
                    {
                        killedTarget = true,
                        target = targets[i].target,
                        targetIndex = targets[i].targetIndex
                    };
                }
            }
        }
    }
}