using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Jobs
{
    [BurstCompile]
    public struct EnemyDestroyJob : IJobChunk
    {
        public EntityCommandBuffer.ParallelWriter CommandBuffer;
        [ReadOnly] public ComponentTypeHandle<HpComponent> hpComponent;
        [ReadOnly] public EntityTypeHandle entity;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var entityTemp = chunk.GetNativeArray(entity);

            var hpTemp = chunk.GetNativeArray(hpComponent);

            for (int i = 0; i < chunk.Count; i++)
            {
                if (hpTemp[i].hp <= 0)
                {
                    CommandBuffer.DestroyEntity(chunkIndex, entityTemp[i]);
                }
            }
        }
    }
}