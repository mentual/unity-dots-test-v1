using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Jobs
{
    [BurstCompile]
    public struct EnemyKamikazeHitJob : IJobChunk
    {
        [ReadOnly] public ComponentTypeHandle<TargetComponent> targetComponent;

        public NativeHashMap<int, int> nexusesToHurt;


        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var count = chunk.Count;

            var targets = chunk.GetNativeArray(targetComponent);

            for (int i = 0; i < count; i++)
            {
                if (!targets[i].killedTarget)
                    return;

                if (nexusesToHurt.ContainsKey(targets[i].targetIndex))
                {
                    nexusesToHurt[targets[i].targetIndex]++;
                }
                else
                {
                    nexusesToHurt.Add(targets[i].targetIndex, 1);
                }
            }
        }
    }
}