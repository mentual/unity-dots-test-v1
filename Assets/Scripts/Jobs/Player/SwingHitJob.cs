using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Jobs
{
    [BurstCompile]
    public struct SwingHitJob : IJobChunk
    {
        [ReadOnly] public float2 swordPointA;
        [ReadOnly] public float2 swordPointB;
        [ReadOnly] public float2 swordPointC;
        [ReadOnly] public ComponentTypeHandle<Translation> targetTranslationComponent;
        public ComponentTypeHandle<HpComponent> targetHpComponent;


        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var targetTranslations = chunk.GetNativeArray(targetTranslationComponent);
            var targetHps = chunk.GetNativeArray(targetHpComponent);

            int count = chunk.Count;

            var swordAB = swordPointB - swordPointA;
            var swordBC = swordPointC - swordPointB;

            for (int i = 0; i < count; i++)
            {
                if (IsTargetInRange(targetTranslations[i].Value, swordAB, swordBC))
                {
                    targetHps[i] = new HpComponent()
                    {
                        hp = 0
                    };
                }
            }
        }

        private bool IsTargetInRange(float3 targetPos, float2 swordAB, float2 swordBC)
        {
            float2 targetPoint = new float2(targetPos.x, targetPos.y);

            var A_Target = targetPoint - swordPointA;
            var B_TARGET = targetPoint - swordPointB;

            bool b1 = 0 <= math.dot(swordAB, A_Target);
            bool b2 = math.dot(swordAB, A_Target) <= math.dot(swordAB, swordAB);
            bool b3 = 0 <= math.dot(swordBC, B_TARGET);
            bool b4 = math.dot(swordBC, B_TARGET) <= math.dot(swordBC, swordBC);

            return b1 && b2 && b3 && b4;
        }
    }
}