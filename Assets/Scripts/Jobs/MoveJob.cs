using Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Jobs
{
    [BurstCompile]
    public struct MoveJob : IJobChunk
    {
        public ComponentTypeHandle<Translation> translationComponent;
        public ComponentTypeHandle<Rotation> rotationComponent;

        [ReadOnly] public float deltaTime;
        [ReadOnly] public ComponentTypeHandle<SpeedComponent> speedComponent;
        [ReadOnly] public ComponentTypeHandle<TargetComponent> targetComponent;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var chunkTranslations = chunk.GetNativeArray(translationComponent);
            var chunkRotations = chunk.GetNativeArray(rotationComponent);
            var chunkTargets = chunk.GetNativeArray(targetComponent);
            var chunkSpeeds = chunk.GetNativeArray(speedComponent);

            for (int i = 0; i < chunk.Count; i++)
            {
                var dir = chunkTargets[i].target - chunkTranslations[i].Value;

                var normalizedDir = math.normalize(dir);

                var step = normalizedDir * chunkSpeeds[i].speed * deltaTime;

                // Handle TR
                chunkTranslations[i] = new Translation
                {
                    Value = chunkTranslations[i].Value + step
                };

                // Handle ROT
                var lookRot = quaternion.LookRotationSafe(normalizedDir, new float3(0f, 0f, -1f));
                chunkRotations[i] = new Rotation
                {
                    Value = math.mul(lookRot, quaternion.RotateX(math.radians(90f)))
                };
            }
        }
    }
}